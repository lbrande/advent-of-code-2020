use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut ntrees = [0; 5];
    let mut xs = [0; 5];
    let xslopes = [1, 3, 5, 7, 1];
    let yslopes = [1, 1, 1, 1, 2];
    for (y, line) in BufReader::new(File::open("input/day_3")?).lines().enumerate() {
        let line = line?;
        for i in 0..5 {
            if y % yslopes[i] == 0 {
                if line.as_bytes()[xs[i] % line.len()] == b'#' {
                    ntrees[i] += 1;
                }
                xs[i] += xslopes[i];
            }
        }
    }
    println!("{}", ntrees.iter().product::<i32>());
    Ok(())
}

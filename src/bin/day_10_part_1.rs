use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut joltages: Vec<i32> = (BufReader::new(File::open("input/day_10")?).lines())
        .map(|line| line.unwrap().parse().unwrap())
        .collect();
    joltages.push(0);
    joltages.sort_unstable();
    joltages.push(joltages.last().unwrap() + 3);
    let mut diffs = [0; 4];
    for i in 1..joltages.len() {
        diffs[(joltages[i] - joltages[i - 1]) as usize] += 1;
    }
    println!("{}", diffs[1] * diffs[3]);
    Ok(())
}

use std::{
    fs::File,
    io::{self, BufRead, BufReader},
    vec,
};

fn main() -> io::Result<()> {
    let mut passports = vec![String::new()];
    for line in BufReader::new(File::open("input/day_4")?).lines().map(Result::unwrap) {
        if line.is_empty() {
            passports.push(String::new());
        } else {
            *passports.last_mut().unwrap() += &line;
        }
    }
    println!("{}", passports.iter().filter(is_valid).count());
    Ok(())
}

fn is_valid(passport: &&String) -> bool {
    let colons = passport.bytes().filter(|&c| c == b':').count();
    colons == 8 || (colons == 7 && !passport.contains("cid:"))
}

use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let numbers: Vec<i64> = (BufReader::new(File::open("input/day_9")?).lines())
        .map(|line| line.unwrap().parse().unwrap())
        .collect();
    for i in 0..numbers.len() {
        let mut sum = 0;
        let mut j = i;
        while sum < 10884537 {
            sum += numbers[j];
            j += 1;
        }
        if sum == 10884537 {
            let min = numbers[i..j].iter().min().unwrap();
            let max = numbers[i..j].iter().max().unwrap();
            println!("{}", min + max);
            break;
        }
    }
    Ok(())
}

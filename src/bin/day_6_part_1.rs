use std::{
    collections::HashSet,
    fs::File,
    io::{self, BufRead, BufReader},
    vec,
};

fn main() -> io::Result<()> {
    let mut groups = vec![HashSet::new()];
    for line in BufReader::new(File::open("input/day_6")?).lines().map(Result::unwrap) {
        if line.is_empty() {
            groups.push(HashSet::new());
        } else {
            for c in line.chars() {
                groups.last_mut().unwrap().insert(c);
            }
        }
    }
    println!("{}", groups.iter().map(HashSet::len).sum::<usize>());
    Ok(())
}

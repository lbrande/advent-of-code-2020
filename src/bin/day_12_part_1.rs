use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut direction = 0;
    let mut north = 0;
    let mut east = 0;
    for line in BufReader::new(File::open("input/day_12")?).lines().map(Result::unwrap) {
        let (action, value) = line.split_at(1);
        let value: i32 = value.parse().unwrap();
        match action {
            "N" => north += value,
            "S" => north -= value,
            "E" => east += value,
            "W" => east -= value,
            "L" => direction = (direction - value / 90 + 4) % 4,
            "R" => direction = (direction + value / 90) % 4,
            _ => match direction {
                0 => east += value,
                1 => north -= value,
                2 => east -= value,
                _ => north += value,
            },
        }
    }
    println!("{}", north.abs() + east.abs());
    Ok(())
}

use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut lines = BufReader::new(File::open("input/day_13")?).lines().map(Result::unwrap);
    let min = lines.next().unwrap().parse().unwrap();
    let bus = (lines.next().unwrap().split(','))
        .filter(|&bus| bus != "x")
        .map(|bus| bus.parse().unwrap())
        .min_by(|&a, &b| departure(a, min).cmp(&departure(b, min)))
        .unwrap();
    println!("{}", bus * departure(bus, min));
    Ok(())
}

fn departure(bus: u32, min: u32) -> u32 {
    bus - min % bus
}

use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut lines = BufReader::new(File::open("input/day_16")?).lines().map(Result::unwrap);
    let mut is_valid = [false; 1024];
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        let line: Vec<_> = line.split(':').collect();
        let delim = |c| c == ' ' || c == '-';
        let values: Vec<usize> = line[1].split(delim).filter_map(|s| s.parse().ok()).collect();
        for n in (values[0]..=values[1]).chain(values[2]..=values[3]) {
            is_valid[n] = true;
        }
    }
    println!("{}", lines.skip(4).map(|line| error_rate(&line, &is_valid)).sum::<usize>());
    Ok(())
}

fn error_rate(line: &str, is_valid: &[bool]) -> usize {
    line.split(',').map(|n| n.parse::<usize>().unwrap()).filter(|&n| !is_valid[n]).sum()
}

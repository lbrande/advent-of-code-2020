use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut north = 1;
    let mut east = 10;
    let mut ship_north = 0;
    let mut ship_east = 0;
    for line in BufReader::new(File::open("input/day_12")?).lines().map(Result::unwrap) {
        let (action, value) = line.split_at(1);
        let value: i32 = value.parse().unwrap();
        match action {
            "N" => north += value,
            "S" => north -= value,
            "E" => east += value,
            "W" => east -= value,
            "L" => {
                for _ in 0..value / 90 {
                    let (north_orig, east_orig) = (north, east);
                    north = east_orig;
                    east = -north_orig;
                }
            }
            "R" => {
                for _ in 0..value / 90 {
                    let (north_orig, east_orig) = (north, east);
                    north = -east_orig;
                    east = north_orig;
                }
            }
            _ => {
                ship_north += north * value;
                ship_east += east * value;
            }
        }
    }
    println!("{}", ship_north.abs() + ship_east.abs());
    Ok(())
}

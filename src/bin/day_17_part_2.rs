use std::{
    collections::HashSet,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut grid: HashSet<_> = (BufReader::new(File::open("input/day_17")?).lines().enumerate())
        .flat_map(|(i, line)| active_cells(i, &line.unwrap()))
        .collect();
    for _ in 0..6 {
        let mut new_grid = HashSet::new();
        for &(x, y, z, w) in &grid {
            if (2..=3).contains(&noccupied((x, y, z, w), &grid)) {
                new_grid.insert((x, y, z, w));
            }
            for dx in -1..=1 {
                for dy in -1..=1 {
                    for dz in -1..=1 {
                        for dw in -1..=1 {
                            let cell = (x + dx, y + dy, z + dz, w + dw);
                            if !grid.contains(&cell) && noccupied(cell, &grid) == 3 {
                                new_grid.insert(cell);
                            }
                        }
                    }
                }
            }
        }
        grid = new_grid;
    }
    println!("{}", grid.len());
    Ok(())
}

fn active_cells(i: usize, line: &str) -> Vec<(i32, i32, i32, i32)> {
    (line.chars().enumerate())
        .filter(|&(_, c)| c == '#')
        .map(|(j, _)| (j as i32, i as i32, 0, 0))
        .collect()
}

fn noccupied((x, y, z, w): (i32, i32, i32, i32), grid: &HashSet<(i32, i32, i32, i32)>) -> u32 {
    let mut noccupied = 0;
    for dx in -1..=1 {
        for dy in -1..=1 {
            for dz in -1..=1 {
                for dw in -1..=1 {
                    if dx != 0 || dy != 0 || dz != 0 || dw != 0 {
                        noccupied += grid.contains(&(x + dx, y + dy, z + dz, w + dw)) as u32;
                    }
                }
            }
        }
    }
    noccupied
}

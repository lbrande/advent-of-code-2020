use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut code = Vec::new();
    for line in BufReader::new(File::open("input/day_8")?).lines().map(Result::unwrap) {
        let (op, arg) = line.split_at(3);
        code.push((op.to_string(), arg.trim().parse().unwrap()));
    }
    for (i, instruction) in code.iter().enumerate() {
        let (op, arg) = instruction.to_owned();
        let mut code = code.clone();
        match op.as_str() {
            "jmp" => code[i] = ("nop".to_string(), arg),
            "nop" => code[i] = ("jmp".to_string(), arg),
            _ => continue,
        }
        let mut acc = 0;
        let mut ptr = 0;
        while (ptr as usize) < code.len() {
            let (op, arg) = code[ptr as usize].to_owned();
            code[ptr as usize] = (String::new(), 0);
            match op.as_str() {
                "acc" => acc += arg,
                "jmp" => ptr += arg - 1,
                "nop" => {}
                _ => break,
            }
            ptr += 1;
        }
        if ptr as usize == code.len() {
            println!("{}", acc);
            break;
        }
    }
    Ok(())
}

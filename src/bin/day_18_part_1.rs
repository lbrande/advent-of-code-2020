use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let sum: u64 = (BufReader::new(File::open("input/day_18")?).lines())
        .map(|line| evaluate(&line.unwrap()))
        .sum();
    println!("{}", sum);
    Ok(())
}

fn evaluate(line: &str) -> u64 {
    let mut value = 0;
    let mut operator = '+';
    let mut stored = Vec::new();
    for token in line.chars().filter(|&c| c != ' ') {
        match token {
            n @ '1'..='9' => match operator {
                '+' => value += n as u64 - '0' as u64,
                _ => value *= n as u64 - '0' as u64,
            },
            '(' => {
                stored.push((value, operator));
                value = 0;
                operator = '+';
            }
            ')' => {
                let (n, op) = stored.pop().unwrap();
                match op {
                    '+' => value += n,
                    _ => value *= n,
                }
            }
            op => operator = op,
        }
    }
    value
}

use std::collections::HashMap;

fn main() {
    let numbers = [9, 3, 1, 0, 8, 4];
    let (indices, last_number) = numbers.split_at(numbers.len() - 1);
    let mut indices: HashMap<_, _> = indices.iter().enumerate().map(|(i, &n)| (n, i)).collect();
    let mut last_number = last_number[0];
    for i in indices.len()..29999999 {
        if let Some(&j) = indices.get(&last_number) {
            indices.insert(last_number, i);
            last_number = i - j;
        } else {
            indices.insert(last_number, i);
            last_number = 0;
        }
    }
    println!("{}", last_number);
}

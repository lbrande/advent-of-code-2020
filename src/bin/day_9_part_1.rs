use std::{
    collections::{HashSet, VecDeque},
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut numbers = VecDeque::new();
    for line in BufReader::new(File::open("input/day_9")?).lines() {
        let i: i64 = line?.parse().unwrap();
        if numbers.len() == 25 {
            let mut checked = HashSet::new();
            for j in &numbers {
                if checked.contains(&(i - j)) {
                    break;
                } else {
                    checked.insert(j);
                }
            }
            if checked.len() == numbers.len() {
                println!("{}", i);
                break;
            }
            numbers.pop_front().unwrap();
            numbers.push_back(i);
        } else {
            numbers.push_back(i);
        }
    }
    Ok(())
}

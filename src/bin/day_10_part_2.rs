use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut joltages: Vec<i32> = (BufReader::new(File::open("input/day_10")?).lines())
        .map(|line| line.unwrap().parse().unwrap())
        .collect();
    joltages.push(0);
    joltages.sort_unstable();
    joltages.push(joltages.last().unwrap() + 3);
    let mut runs = vec![0];
    for i in 1..joltages.len() {
        match joltages[i] - joltages[i - 1] {
            1 => *runs.last_mut().unwrap() += 1,
            _ => runs.push(0),
        }
    }
    let mut narrangments = vec![[1; 3]; 2];
    for i in 2..=*runs.iter().max().unwrap() {
        narrangments.push([narrangments[i - 1][0]; 3]);
        narrangments[i][0] += narrangments[i - 1][1];
        narrangments[i][1] += narrangments[i - 1][2];
    }
    println!("{}", runs.iter().map(|&run| narrangments[run][0]).product::<u64>());
    Ok(())
}

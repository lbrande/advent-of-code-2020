use std::{
    collections::HashSet,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut numbers = HashSet::new();
    for line in BufReader::new(File::open("input/day_1")?).lines() {
        let i: i32 = line?.parse().unwrap();
        if let Some(j) = numbers.iter().find_map(|j| numbers.get(&(2020 - i - j))) {
            println!("{}", i * j * (2020 - i - j));
            break;
        } else {
            numbers.insert(i);
        }
    }
    Ok(())
}

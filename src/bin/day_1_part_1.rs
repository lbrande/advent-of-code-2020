use std::{
    collections::HashSet,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut numbers = HashSet::new();
    for line in BufReader::new(File::open("input/day_1")?).lines() {
        let i: i32 = line?.parse().unwrap();
        if numbers.contains(&(2020 - i)) {
            println!("{}", i * (2020 - i));
            break;
        } else {
            numbers.insert(i);
        }
    }
    Ok(())
}

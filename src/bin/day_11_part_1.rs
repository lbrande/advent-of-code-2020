use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut grid: Vec<Vec<_>> = (BufReader::new(File::open("input/day_11")?).lines())
        .map(|line| line.unwrap().chars().collect())
        .collect();
    loop {
        let mut new_grid = grid.clone();
        let mut is_changed = false;
        for row in 0..grid.len() {
            for col in 0..grid[0].len() {
                let noccupied = noccupied(row, col, &grid);
                if noccupied == 0 && grid[row][col] == 'L' {
                    new_grid[row][col] = '#';
                    is_changed = true;
                } else if noccupied >= 4 && grid[row][col] == '#' {
                    new_grid[row][col] = 'L';
                    is_changed = true;
                }
            }
        }
        grid = new_grid;
        if !is_changed {
            break;
        }
    }
    println!("{}", grid.iter().flatten().map(|&c| (c == '#') as u32).sum::<u32>());
    Ok(())
}

fn noccupied(row: usize, col: usize, grid: &[Vec<char>]) -> u32 {
    let mut noccupied = 0;
    for drow in -1..=1 {
        for dcol in -1..=1 {
            if drow != 0 || dcol != 0 {
                noccupied += is_token('#', row as i32 + drow, col as i32 + dcol, grid) as u32;
            }
        }
    }
    noccupied
}

fn is_token(token: char, row: i32, col: i32, grid: &[Vec<char>]) -> bool {
    row >= 0
        && row < grid.len() as i32
        && col >= 0
        && col < grid[0].len() as i32
        && grid[row as usize][col as usize] == token
}

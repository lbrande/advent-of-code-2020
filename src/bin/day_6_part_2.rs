use std::{
    collections::HashSet,
    fs::File,
    io::{self, BufRead, BufReader},
    vec,
};

fn main() -> io::Result<()> {
    let mut groups = vec![Vec::new()];
    for line in BufReader::new(File::open("input/day_6")?).lines().map(Result::unwrap) {
        if line.is_empty() {
            groups.push(Vec::new());
        } else {
            groups.last_mut().unwrap().push(line.chars().collect());
        }
    }
    println!("{}", groups.iter().map(|group| group_value(group)).sum::<usize>());
    Ok(())
}

fn group_value(group: &[HashSet<char>]) -> usize {
    group.iter().fold(('a'..='z').collect(), |a: HashSet<_>, b| &a & b).len()
}

use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut nvalid = 0;
    for line in BufReader::new(File::open("input/day_2")?).lines().map(Result::unwrap) {
        let delim = |c| c == '-' || c == ' ' || c == ':';
        let line: Vec<_> = line.split(delim).filter(|s| !s.is_empty()).collect();
        let first_pos: usize = line[0].parse().unwrap();
        let second_pos: usize = line[1].parse().unwrap();
        let token = line[2].as_bytes()[0];
        let password = line[3].as_bytes();
        if (password[first_pos - 1] == token) ^ (password[second_pos - 1] == token) {
            nvalid += 1;
        }
    }
    println!("{}", nvalid);
    Ok(())
}

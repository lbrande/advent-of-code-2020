use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut bags = HashMap::new();
    for line in BufReader::new(File::open("input/day_7")?).lines().map(Result::unwrap) {
        let line: Vec<_> = line.split_whitespace().collect();
        let outer = format!("{} {}", line[0], line[1]);
        let mut contents = Vec::new();
        for i in (5..(line.len() - 2)).step_by(4) {
            let ninner: u32 = line[i - 1].parse().unwrap();
            let inner = format!("{} {}", line[i], line[i + 1]);
            contents.push((ninner, inner));
        }
        bags.insert(outer, contents);
    }
    println!("{}", ncontained("shiny gold", &bags, &mut HashMap::new()));
    Ok(())
}

fn ncontained(
    bag: &str, bags: &HashMap<String, Vec<(u32, String)>>, memo: &mut HashMap<String, u32>,
) -> u32 {
    if let Some(&ncontained) = memo.get(bag) {
        ncontained
    } else {
        let ncontained =
            bags[bag].iter().map(|(n, bag)| n * (1 + ncontained(&bag, bags, memo))).sum();
        memo.insert(bag.to_string(), ncontained);
        ncontained
    }
}

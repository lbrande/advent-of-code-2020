use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut bags = HashMap::new();
    for line in BufReader::new(File::open("input/day_7")?).lines().map(Result::unwrap) {
        let line: Vec<_> = line.split_whitespace().collect();
        let outer = format!("{} {}", line[0], line[1]);
        for i in (5..(line.len() - 2)).step_by(4) {
            let inner = format!("{} {}", line[i], line[i + 1]);
            bags.entry(inner).or_insert_with(Vec::new).push(outer.clone());
        }
    }
    let mut stack = vec!["shiny gold"];
    let mut visited = HashSet::new();
    while let Some(bag) = stack.pop() {
        visited.insert(bag);
        for bag in bags.get(bag).map(Vec::as_slice).unwrap_or(&[]) {
            if !visited.contains(bag.as_str()) {
                stack.push(bag);
            }
        }
    }
    println!("{}", visited.len() - 1);
    Ok(())
}

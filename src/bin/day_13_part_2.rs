use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let buses = BufReader::new(File::open("input/day_13")?).lines().nth(1).unwrap().unwrap();
    let t0 = (buses.split(',').enumerate())
        .filter(|&(_, bus)| bus != "x")
        .map(|(id, bus)| (id, bus.parse().unwrap()))
        .fold((0, 1), |(t0, dt), (id, bus)| {
            let lcm = lcm(dt, bus);
            ((t0 + dt * solve(-dt, bus, t0 + id as i128).0).rem_euclid(lcm), lcm)
        })
        .0;
    println!("{}", t0);
    Ok(())
}

#[allow(clippy::many_single_char_names)]
fn solve(a: i128, b: i128, c: i128) -> (i128, i128) {
    let rem = a.rem_euclid(b);
    if rem == 0 {
        (0, c / b)
    } else {
        let (x, y) = solve(b, rem, c);
        (y, x - a.div_euclid(b) * y)
    }
}

fn lcm(a: i128, b: i128) -> i128 {
    (a * b).abs() / gcd(a, b)
}

fn gcd(a: i128, b: i128) -> i128 {
    let rem = a.rem_euclid(b);
    if rem == 0 {
        b
    } else {
        gcd(b, rem)
    }
}

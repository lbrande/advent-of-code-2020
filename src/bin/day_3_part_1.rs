use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut ntrees = 0;
    let mut x = 0;
    for line in BufReader::new(File::open("input/day_3")?).lines().map(Result::unwrap) {
        if line.as_bytes()[x % line.len()] == b'#' {
            ntrees += 1;
        }
        x += 3;
    }
    println!("{}", ntrees);
    Ok(())
}

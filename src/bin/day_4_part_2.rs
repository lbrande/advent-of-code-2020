use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
    vec,
};

fn main() -> io::Result<()> {
    let mut passports = vec![HashMap::new()];
    for line in BufReader::new(File::open("input/day_4")?).lines().map(Result::unwrap) {
        if line.is_empty() {
            passports.push(HashMap::new());
        } else {
            for field in line.split_whitespace().map(|field| field.split(':').collect::<Vec<_>>()) {
                passports.last_mut().unwrap().insert(field[0].to_string(), field[1].to_string());
            }
        }
    }
    println!("{}", passports.iter().filter(is_valid).count());
    Ok(())
}

fn is_valid(passport: &&HashMap<String, String>) -> bool {
    let nvalid_fields = passport.iter().filter(is_valid_field).count();
    nvalid_fields == 8 || (nvalid_fields == 7 && !passport.contains_key("cid"))
}

fn is_valid_field((key, value): &(&String, &String)) -> bool {
    match key.as_str() {
        "byr" => {
            let byr: u32 = value.parse().unwrap();
            (1920..=2002).contains(&byr)
        }
        "iyr" => {
            let iyr: u32 = value.parse().unwrap();
            (2010..=2020).contains(&iyr)
        }
        "eyr" => {
            let eyr: u32 = value.parse().unwrap();
            (2020..=2030).contains(&eyr)
        }
        "hgt" => {
            if let Some(hgt) = value.strip_suffix("cm") {
                let hgt: u32 = hgt.parse().unwrap();
                (150..=193).contains(&hgt)
            } else if let Some(hgt) = value.strip_suffix("in") {
                let hgt: u32 = hgt.parse().unwrap();
                (59..=76).contains(&hgt)
            } else {
                false
            }
        }
        "hcl" => match value.strip_prefix('#') {
            Some(hcl) => hcl.len() == 6 && u32::from_str_radix(hcl, 16).is_ok(),
            None => false,
        },
        "ecl" => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&value.as_str()),
        "pid" => value.len() == 9 && value.chars().all(|c| c.is_numeric()),
        _ => true,
    }
}

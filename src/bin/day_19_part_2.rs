use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut lines = BufReader::new(File::open("input/day_19")?).lines().map(Result::unwrap);
    let mut rules: HashMap<u32, Rule> = HashMap::new();
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        let line: Vec<_> = line.split(": ").collect();
        rules.insert(line[0].parse().unwrap(), rule(line[1]));
    }
    rules.insert(8, rule("42 | 42 8"));
    rules.insert(11, rule("42 31 | 42 11 31"));
    println!("{}", lines.filter(|line| match_suffixes(line, 0, &rules).contains(&"")).count());
    Ok(())
}

fn match_suffixes<'a>(line: &'a str, rule: u32, rules: &HashMap<u32, Rule>) -> Vec<&'a str> {
    match &rules[&rule] {
        Rule::Literal(c) => line.strip_prefix(c).iter().copied().collect(),
        Rule::Variable(exprs) => {
            exprs.iter().flat_map(|expr| match_expr(line, expr, rules)).collect()
        }
    }
}

fn match_expr<'a>(line: &'a str, expr: &[u32], rules: &HashMap<u32, Rule>) -> Vec<&'a str> {
    expr.iter().fold(vec![line], |suffixes, &atom| {
        suffixes.iter().flat_map(|suffix| match_suffixes(suffix, atom, rules)).collect()
    })
}

fn rule(string: &str) -> Rule {
    match string.strip_prefix('"').and_then(|c| c.strip_suffix('"')) {
        Some(c) => Rule::Literal(c.to_string()),
        None => Rule::Variable(string.split(" | ").map(atoms).collect()),
    }
}

fn atoms(string: &str) -> Vec<u32> {
    string.split_whitespace().map(|n| n.parse().unwrap()).collect()
}

#[derive(Clone, Debug)]
enum Rule {
    Literal(String),
    Variable(Vec<Vec<u32>>),
}

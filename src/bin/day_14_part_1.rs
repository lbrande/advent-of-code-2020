use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut set_mask = 0;
    let mut clr_mask = 0;
    let mut mem = HashMap::new();
    for line in BufReader::new(File::open("input/day_14")?).lines().map(Result::unwrap) {
        let delim = |c| c == ' ' || c == '=' || c == '[' || c == ']';
        let line: Vec<_> = line.split(delim).filter(|s| !s.is_empty()).collect();
        if line[0] == "mask" {
            let mask = line[1];
            set_mask = mask.chars().fold(0, |mask, c| (mask << 1) + (c == '1') as u64);
            clr_mask = mask.chars().fold(0, |mask, c| (mask << 1) + (c != '0') as u64);
        } else {
            let addr: u64 = line[1].parse().unwrap();
            let value: u64 = line[2].parse().unwrap();
            mem.insert(addr, (value | set_mask) & clr_mask);
        }
    }
    println!("{}", mem.values().sum::<u64>());
    Ok(())
}

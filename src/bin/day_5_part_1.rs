use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let max_id = (BufReader::new(File::open("input/day_5")?).lines())
        .map(|seat| id(&seat.unwrap()))
        .max()
        .unwrap();
    println!("{}", max_id);
    Ok(())
}

fn id(seat: &str) -> u32 {
    seat.chars().fold(0, |id, c| (id << 1) + (c == 'B' || c == 'R') as u32)
}

use std::{
    collections::HashSet,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut lines = BufReader::new(File::open("input/day_16")?).lines().map(Result::unwrap);
    let mut is_valid = [false; 1024];
    let mut fields = Vec::new();
    while let Some(line) = lines.next() {
        if line.is_empty() {
            break;
        }
        let line: Vec<_> = line.split(':').collect();
        let delim = |c| c == ' ' || c == '-';
        let values: Vec<usize> = line[1].split(delim).filter_map(|s| s.parse().ok()).collect();
        let mut is_valid_field = [false; 1024];
        for n in (values[0]..=values[1]).chain(values[2]..=values[3]) {
            is_valid[n] = true;
            is_valid_field[n] = true;
        }
        fields.push((line[0].to_string(), is_valid_field));
    }
    lines.next();
    let ticket: Vec<usize> = lines.next().unwrap().split(',').map(|n| n.parse().unwrap()).collect();
    let tickets: Vec<_> = (lines.skip(2))
        .map(|line| ticket_from_line(&line))
        .filter(|ticket| ticket.iter().all(|&n| is_valid[n]))
        .collect();
    let mut valid_field_indices: Vec<_> = (fields.iter().enumerate())
        .map(|(i, (_, is_valid))| (i, valid_field_indices(&is_valid, &tickets)))
        .collect();
    valid_field_indices.sort_unstable_by(|a, b| a.1.len().cmp(&b.1.len()));
    let mut assigned_indices = HashSet::new();
    let mut field_indices = vec![0; fields.len()];
    for (i, valid_field_indices) in valid_field_indices {
        let index = *(valid_field_indices.iter())
            .find(|&indices| !assigned_indices.contains(indices))
            .unwrap();
        field_indices[i] = index;
        assigned_indices.insert(index);
    }
    let product: usize = (fields.iter().enumerate())
        .filter(|(_, (name, _))| name.starts_with("departure"))
        .map(|(i, _)| ticket[field_indices[i]])
        .product();
    println!("{}", product);
    Ok(())
}

fn ticket_from_line(line: &str) -> Vec<usize> {
    line.split(',').map(|n| n.parse().unwrap()).collect()
}

fn valid_field_indices(is_valid: &[bool; 1024], tickets: &[Vec<usize>]) -> Vec<usize> {
    (0..tickets[0].len()).filter(|&i| tickets.iter().all(|ticket| is_valid[ticket[i]])).collect()
}

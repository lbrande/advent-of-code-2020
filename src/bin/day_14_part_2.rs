use std::{
    collections::HashMap,
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut set_mask = 0;
    let mut floating_bits = Vec::new();
    let mut mem = HashMap::new();
    for line in BufReader::new(File::open("input/day_14")?).lines().map(Result::unwrap) {
        let delim = |c| c == ' ' || c == '=' || c == '[' || c == ']';
        let line: Vec<_> = line.split(delim).filter(|s| !s.is_empty()).collect();
        if line[0] == "mask" {
            let mask = line[1];
            set_mask = mask.chars().fold(0, |mask, c| (mask << 1) + (c == '1') as u64);
            floating_bits = (mask.chars().rev().enumerate())
                .filter(|&(_, c)| c == 'X')
                .map(|(i, _)| i)
                .collect();
        } else {
            let addr: u64 = line[1].parse().unwrap();
            let value: u64 = line[2].parse().unwrap();
            for floating in 0..(1 << floating_bits.len()) {
                let mut addr = addr;
                for (i, bit) in floating_bits.iter().enumerate() {
                    match (floating >> i) & 1 {
                        0 => addr &= (1 << bit) ^ u64::MAX,
                        _ => addr |= 1 << bit,
                    }
                }
                mem.insert(addr | set_mask, value);
            }
        }
    }
    println!("{}", mem.values().sum::<u64>());
    Ok(())
}

use std::{
    fs::File,
    io::{self, BufRead, BufReader},
};

fn main() -> io::Result<()> {
    let mut ids: Vec<_> = (BufReader::new(File::open("input/day_5")?).lines())
        .map(|seat| id(&seat.unwrap()))
        .collect();
    ids.sort_unstable();
    for i in 1..ids.len() {
        if ids[i] - ids[i - 1] == 2 {
            println!("{}", ids[i] - 1);
        }
    }
    Ok(())
}

fn id(seat: &str) -> u32 {
    seat.chars().fold(0, |id, c| (id << 1) + (c == 'B' || c == 'R') as u32)
}
